<?php
require 'config.php';
$projectname=$_GET["projectname"];
$credits=$_GET["credits"];
try
  {
    $conn= new PDO("mysql:host=$servername;dbname=$database;charset:utf8",$username,$password);
    $conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $stmt= $conn->prepare("UPDATE projects SET credits=:credits WHERE name =:projectname");
    $stmt-> bindParam(":credits",$credits);
    $stmt-> bindParam(":projectname",$projectname);
    $stmt-> execute();
    }
 catch(PDOException $e){
      echo $e;
    }
?>
