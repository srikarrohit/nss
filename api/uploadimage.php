<?php
	require 'config.php';
	$err= [true,''];
	function imagevalidation($err)
	{
		$allowedExts = array("jpg","jpeg","gif","png");
		$array= explode(".",$_FILES["photo1"]["name"]);
		$extension= strtolower(end($array));
		if($_FILES["photo1"]["name"] == NULL)
		{
			if($err)
			{
				$err[1]=' '; 
			}
			$err[1]= $err[1]."Image compulsory";
			$err[0]= false;
			return $err;
		}
		elseif(strpos($_FILES["photo1"]["name"],'.htaccess') !== false) // Prevention of entry of .htaccess files into the upload folder.  
		{
			if($err)
			{
				$err[1]=' '; 
			}
			$err[1]= $err[1]."Invalid file type(Image)";
			$err[0]= false;
			return $err;
		}
		elseif(($_FILES['photo1']['type']!=='image/gif')&&($_FILES['photo1']['type']!=='image/png')&& ($_FILES['photo1']['type']!=='image/jpg') &&($_FILES['photo1']['type'] !=='image/jpeg'))
		{
			if($err)
			{
				$err[1]=' '; 
			}
			$err[1]= $err[1]."Invalid file type(Image)";
			$err[0]= false;
			return $err;
		}
		elseif(!in_array($extension, $allowedExts))
		{
			if($err)
			{
				$err[1]=' '; 
			}
			$err[1]= $err[1]."Invalid Extension(Image)";
			$err[0]= false;
			return $err;
		}
		elseif($_FILES["photo1"]["size"] > 5242880) //File size should be less than 5MegaBytes
		{
			if($err)
			{
				$err[1]=' '; 
			}
			$err[1]= $err[1]."File size should be less than 5 MegaBytes";
			$err[0]= false;
			return $err;
		}
		else
		{
			$target_path= 'C://xampp1/htdocs/nss/img/';
			$target_path= $target_path.$_FILES["photo1"]["name"]; //String Concatenation
			$err[1]= $target_path;
			return $err;
		}
	}
	$finalerr= imagevalidation($err);
	if($finalerr[0])
	{
		if(move_uploaded_file($_FILES['photo1']['tmp_name'],$finalerr[1]))
		{
			$conn= new PDO("mysql:host=$servername;dbname=nss;charset=utf8",$username,$password);
			$conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$stmt= $conn->prepare("INSERT INTO images(name,location) VALUES(:name,:location)");
			$stmt->bindParam(':name',$_FILES['photo1']['name']);
			$stmt->bindParam(':location',str_replace("C://xampp1/htdocs","",$finalerr[1]));
			$stmt->execute();
			echo "Success,".$finalerr[1] ;
		}
	}
	else
	{
		echo "Failure,".$finalerr[1];
	}
?>
