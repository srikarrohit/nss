<?php
require 'config.php';
$cat =$_GET["cat"];
$cat=htmlspecialchars_decode($cat);
try
  {
    $conn= new PDO("mysql:host=$servername;dbname=$database;charset:utf8",$username,$password);
    $conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $stmt= $conn->prepare("SELECT * FROM projects WHERE category=:cat");
    $stmt-> bindParam(":cat",$cat);
    $stmt-> execute();
    }
 catch(PDOException $e){
      echo $e;
    }
    while($row= $stmt->fetch(PDO::FETCH_ASSOC)) {
    	$event_name = $row["name"];
        $a=array('projectname'=> $event_name);
	$arr[]=$a;
    }
echo json_encode($arr);
?>
