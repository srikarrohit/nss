<?php
$id=$_GET["id"];
require 'config.php';
try
  {     
    $conn= new PDO("mysql:host=$servername;dbname=$database;charset:utf8",$username,$password);
    $conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $stmt= $conn->prepare("SELECT * FROM leaders WHERE user_id=:id");
    $stmt-> bindParam(":id",$id);
    $stmt-> execute();
    $result1= $stmt->fetch(PDO::FETCH_ASSOC);
    }
 catch(PDOException $e){
      echo $e;
    }
if ($result1) {
   echo true;
} else {
    echo false;
}
?>
