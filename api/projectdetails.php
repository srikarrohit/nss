<?php
require 'config.php';
$projectname = $_GET["projectname"];
$projectname = htmlspecialchars_decode($projectname);
try
  {
    $conn= new PDO("mysql:host=$servername;dbname=$database;charset:utf8",$username,$password);
    $conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $stmt= $conn->prepare("SELECT * FROM projects WHERE name=:name");
    $stmt-> bindParam(":name",$projectname);
    $stmt-> execute();
   }
 catch(PDOException $e){
      echo $e;
    }
	while($row1= $stmt->fetch(PDO::FETCH_ASSOC)){
        $project_name=$row1['name'];
        $project_aim=$row1['aim'];
        $project_description=$row1['description'];
        $project_motivation=$row1['motivation'];
        $project_credits=$row1['credits'];
        $a[]=array('projectname'=> $project_name,'aim'=>$project_aim,'description'=>$project_description,'motivation' => $project_motivation,'credits' => $project_credits);     
    }
echo json_encode($a);
?>
