<?php

require 'config.php';
$conn = new mysqli($servername, $username, $password,$database);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql="SELECT * FROM projects";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
    	$event_name = $row["name"];
    	$event_credits=$row["credits"];
    	$event_description=$row["description"];
        $a=array('eventname'=> $event_name,'description'=> $event_description,'credits' => $event_credits);
	$arr[]=$a;
   }
}
else {
    echo "0 results";
}
echo json_encode($arr);
$conn->close();
?>
