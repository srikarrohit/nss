<?php
$id=$_GET["id"];
require 'config.php';
try
  {
    $conn= new PDO("mysql:host=$servername;dbname=$database;charset:utf8",$username,$password);
    $conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $stmt= $conn->prepare("SELECT * FROM volunteers WHERE user_id =:id");
    $stmt-> bindParam(":id",$id);
    $stmt-> execute();
    }
 catch(PDOException $e){
      echo $e;
    }
$credits=0;
  while($row1= $stmt->fetch(PDO::FETCH_ASSOC)){
		$credits+=$row1["earned_credits"];
	}

echo json_encode(array("project_credits"=>$credits));
?>
