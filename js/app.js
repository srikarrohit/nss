//main js file routing is done here
var nssApp = angular.module('nssApp',[
	'ngRoute','nssControllers','mm.foundation'
	]);  
	nssApp.directive('appFilereader', function($q) {
    var slice = Array.prototype.slice;
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, element, attrs, ngModel) {
        if (!ngModel) return;
        ngModel.$render = function() {}
        element.bind('change', function(e) {
          var element = e.target;
          if(!element.value) return;
          element.disabled = true;
          $q.all(slice.call(element.files, 0).map(readFile))
            .then(function(values) {
              if (element.multiple) ngModel.$setViewValue(values);
              else ngModel.$setViewValue(values.length ? values[0] : null);
              element.value = null;
              element.disabled = false;
            });
          function readFile(file) {
            var deferred = $q.defer();
            var reader = new FileReader()
            reader.onload = function(e) {
              deferred.resolve(e.target.result);
            }
            reader.onerror = function(e) {
              deferred.reject(e);
            }
            reader.readAsDataURL(file);
            return deferred.promise;
          }
        });
      }
    };
  })
;
nssApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
			when('/', {
        templateUrl: 'partials/homepage.html',
        controller: 'HomeCtrl'
      }).
      when('/login', {
        templateUrl: 'partials/index.html',
        controller: 'LoginCtrl'
      }).
      when('/loginform',{
        templateUrl: 'partials/loginform.html',
        controller: 'LoginCtrl'  
      }).
			when('/profile', {
        templateUrl: 'profile/index.html',
        controller: 'profileCtrl'
      }).
			when('/upload_credits', {
        templateUrl: 'partials/upload.html',
        controller: 'uploadCtrl'
      }).
      when('/winterns',{
        templateUrl: 'partials/winterns.html',
        controller: 'WinternsCtrl'
      }).
      when('/projects',{
        templateUrl: 'partials/projects.html',
        controller: 'ProjectsCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
  }]);
