//controllers js file
var Controllers = angular.module('nssControllers',['ngCookies']);
var root="http://localhost/nss";
// Start HomeCtrl
Controllers.controller('HomeCtrl',['$scope','$http','$cookies','$location',function($scope,$http,$cookies,$location) {
	$scope.user={};
	$scope.userid=$cookies.get('id');
	$scope.fullname=$cookies.get('fullname');
	$scope.usertype=$cookies.get('usertype');
	console.log($cookies.get('useradmin'));
	if($cookies.get('useradmin')!=null || $cookies.get('useradmin')!=undefined)
		$scope.useradmin=$cookies.get('useradmin');	
	else
		$scope.useradmin=false;

	console.log($scope.useradmin);
	if($scope.userid!=undefined)
	{
      $scope.loggedin=true;
    }
    else
    {
      $scope.loggedin=false;
    }
	console.log("1"+$scope.loggedin);
	window.onload= loadajaximage();
		function loadajaximage(){
			console.log("hello!!");	
				document.getElementById('user_files').addEventListener('change',function(e){ //Ajax File Upload. 
				var file= this.files[0];
				console.log(file);
				var xhr= new XMLHttpRequest();
				var url= root+'/api/uploadimage.php';
				xhr.file= file;
				xhr.onreadystatechange = function(e){
					if(4== this.readyState){
						console.log(['xhr upload complete',e]);
						var status= xhr.responseText.split(',');
						console.log(status);
						if(status[0]=='Success')
						{
							location.reload();
						}
						else
						{
							alert("Upload Failed because of "+ status[1]);
						}
					}
				};
				var fd= new FormData;
				fd.append('photo1',file);
				xhr.open('POST',url,true);
				xhr.send(fd);
			},false);
		}
//    if($scope.useradmin)
//    {
    	$http({
        	method  : 'GET',
        	url     : root+'/api/images.php',
        	headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      	})
		.success(function(data){
			$scope.slides=data;
		//	console.log(data);
			$scope.locations=[];
			var table= document.getElementById('imagetable');
			i=0;
			while(data[i] != undefined)
			{
				var arr= data[i].split("@$#");
				arr[1]= arr[1].replace('../',' ');
				var tr= document.createElement('tr');
				var td0= document.createElement('td');
				td0.innerHTML= i+1;
				tr.appendChild(td0);
				var td1= document.createElement('td');
				var img = document.createElement("img");
    			img.src = arr[1];
    			img.width = 200;
    			img.height = 200;
    			td1.appendChild(img);
    			tr.appendChild(td1);
    			var td2= document.createElement('td');
    			var button = document.createElement('button');
    			button.setAttribute('class','tablebutton');
    			var id= 'image'+ arr[0];
    			button.setAttribute('id',id);
    			button.innerHTML= 'Delete';
    			td2.appendChild(button);
    			tr.appendChild(td2);
    			table.appendChild(tr);
				button.addEventListener('click',function(e){
					var id= this.id;
		//			console.log("hi"+id);
					$http({
	        			method  : 'GET',
	        			url     : root+'/api/images.php?id='+id,
	        			headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
	      			})
	      			.success(function(data0){
//	      			console.log(data0);
	      				location.reload();
	      			})
				})    			 
				i++;
			}
			console.log($scope.locations);
	});
//	}	
		$scope.RemoveCookie = function (){
				$scope.useradmin=false;
				$cookies.remove('usertype1');
				$cookies.remove('useradmin');
        $cookies.remove('fullname');
				$cookies.remove('hostel');
				$cookies.remove('id');
        $cookies.remove('usertype');
        $scope.fullname='';
        $scope.userid='';
    };

		$scope.logout=function(){
			$scope.RemoveCookie();
//			console.log("test");
			$scope.loggedin=false;
	//		$location.path('/');
		};

$http({
        method  : 'GET',
        url     : root+'/api/images.php',
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      })
			.success(function(data){
				$scope.slides=data;
//				console.log(data);
			});
	//};
		$scope.login = function(){
			$location.path("/login");
		};
		$scope.editnewsfeed = function(){
			document.getElementById("newsfeed").contentEditable = true;
		};
		$scope.updatenewsfeed = function(){
			console.log(document.getElementById("newsfeed").innerHTML);
			document.getElementById("newsfeed").contentEditable = false;
			$http.get(root+"/api/updatenewsfeed.php?content="+document.getElementById("newsfeed").innerHTML)
			.success(function(){	
				alert("Succesfully updated!");
				newsfeedload();
			})
			.error(function(){
				alert("Failed to update");
			});
		};


var newsfeedload = function(){
console.log("1");
$http.get(root+"/api/newsfeed.php")

.success(function(data){
	$scope.newsfeed = data;
})

.error(function(){
	console.log("Failed to fetch");
});

};

newsfeedload();
}]);
// End HomeCtrl
Controllers.controller('WinternsCtrl',['$scope','$http','$cookies','$location',function($scope,$http,$cookies,$location) {
$http.get(root+"/api/winterns.php")
	.success(function(data){
		console.log(data);
		$scope.winterns = data;
	})
$scope.winternname = "NSS Winterns(Winter+Interns):";
$scope.winterndescription ="During the month of December, after the start of NSS Activities , All Volunteers,Friends of NSS,Defaulters are given an opportunity to work with an NGO over the period of a week or so. You are required to apply for the same, giving your preference order of NGOs. This is one of the best aspects of working with NSS. Every year, volunteers have an exciting and stimulating time working with dedicated people in various NGOs." ;
$scope.winternwebsite = "";	
$scope.showwinterndetails = function(w1,w2,w3){
$scope.winternname = w1;
$scope.winterndescription = w2;
$scope.winternwebsite = w3;
}
}]);
Controllers.controller('LoginCtrl',['$scope','$http','$cookies','$location',function($scope,$http,$cookies,$location) {
// first the code othere than submit function is loaded synchronously then the code other than httpss request is done and after the information is fetched the code inside success runs. The order is 1,5,4,3
// the cookies usage is changes since angular 1.4. They need to be accessed as below
		if($cookies.get('id')== undefined)
		{
			$scope.user={};
			$scope.userid=$cookies.get('id');
			$scope.fullname=$cookies.get('fullname');
			$scope.usertype=$cookies.get('usertype');
			//$scope.usertype1= "Random";
			$http({
	        method  : 'GET',
	        url     : root+'/api/leaders.php?id='+$scope.userid,
	        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
	      })
				.success(function(data){
					$scope.leader=data;
	//				console.log(data);
				});
			$scope.updatecredit= function(eventname,credits){
			$http.get(root+"/api/puller1.php?eventname="+eventname+"&credits="+credits)
			.success(function(data){
					alert("Record succesfully updated");
	//			console.log(data);
			})
			};
	//		console.log(1);

		/*	if ($scope.userid=="3672" || $scope.userid=="8878") {
				$scope.useradmin= true;
			}
			else{
				$scope.useradmin= false;
			}*/
			$scope.RemoveCookie = function () {
				$cookies.remove('usertype1');
				$cookies.remove('fullname');
				$cookies.remove('hostel');
				$cookies.remove('id');
				$cookies.remove('usertype');
				$scope.fullname='';
				$scope.userid='';
			};
		$scope.volunteer = function(){
			$cookies.put('usertype1','volunteer'); 
			//console.log($scope.usertype1);
			$location.path("/loginform");
		};
		$scope.pr = function(){
			$cookies.put('usertype1','leader');
			$location.path("/loginform");
		};
		$scope.manager = function(){
			$cookies.put('usertype1','manager');
			$location.path("/loginform");
		};
		$scope.submit= function(){
		$http({
		method  : 'POST',
		url     : 'https://students.iitm.ac.in/mobapi/ldap/login.php',
		data    : $.param($scope.user), 
		headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
		})
		.success(function(data){
	//		console.log(data[0].id);
			$scope.usertype1=$cookies.get('usertype1');
			$http.get(root+"/api/check.php?id="+ data[0].id)
			.success(function(data0){
				$scope.usertype = data0;		
			/*	if( data[0].id=="3672"|| data[0].id=="8878")
				{
					$scope.useradmin= true;
				}
				else
				{
					$scope.useradmin= false;
				}*/
		//		console.log($scope.usertype + ' 111 ');
		//		console.log($scope.usertype + ' 111 '+ $scope.usertype1);
				if($scope.usertype == $scope.usertype1)
				{
					$cookies.put('id',data[0].id);
					$cookies.put('username',data[0].username);
					$cookies.put('fullname',data[0].fullname);
					$cookies.put('hostel',data[0].hostel);
					//$cookies.remove('usertype1');
					$scope.userid=$cookies.get('id');
					$scope.fullname=$cookies.get('fullname');
					if($scope.usertype =='leader')
					{
						$http({
							method  : 'GET',
							url     : 'httpss://nss.iitm.ac.in//api/leaders.php?id='+$scope.userid,
							headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
							})
							.success(function(data){
								$scope.leader=data;
							//	$cookies.put('leader',true);
						});
					}
					else if($scope.usertype == "manager"){
						$cookies.put('useradmin',true);
					}
					else if(data0 == "leader"){
					//	$cookies.put('volunteer',true);
					}
/*					if($scope.userid != null)
					{
						$http.get('https://nss.iitm.ac.in//api/puller2.php').success(function(data){//fetching events json data
							$scope.events = data;
						});
					}*/
					$location.path('/');						
				}
				else
				{
					alert("You are not allowed to login as " + $scope.usertype1);
				}
			})
			.error(function(){
			alert("Unable to connect");
			});
		})
		.error(function(data){
		alert("Failed to login! Please enter correct credentials");
		})
		//		console.log(4);
		}
		//	console.log(5);
		}
		else
		{
			$location.path('/');
		}
	}
]);

Controllers.controller('profileCtrl',['$scope','$http','$cookies','$location',function($scope,$http,$cookies,$location){

	$scope.RemoveCookie = function () {
		$cookies.remove('useradmin');
		$cookies.remove('usertype1');
		$cookies.remove('fullname');
    $cookies.remove('hostel');
		$cookies.remove('id');
		$cookies.remove('usertype');
		$cookies.remove('usertype1');
		$scope.fullname='';
		$scope.useradmin=false;
		$scope.userid='';
		$scope.leader=false;
		$location.path('/');
	}
	$scope.id=$cookies.get("id");
//	console.log($scope.id);
	$scope.rollno=$cookies.get("username");
	$scope.fullname=$cookies.get("fullname");
	$scope.hostel=$cookies.get("hostel");
	$scope.usertype=$cookies.get("usertype");
	$scope.leader=false;
	
		if($cookies.get("usertype1")=="leader")
  		$scope.leader=true;
  	else
  		$scope.leader=false;
		console.log($scope.leader);
	console.log($cookies.get("usertype1"));
/*	 $http.get('https://nss.iitm.ac.in//api/leaders.php?id='+$scope.id)
    .success(function(data){
    	$scope.leader=data;
//        console.log(data);
    });*/

	if ($scope.id != null) {
	 $http.get(root+'/api/geteventcredits.php?id='+$scope.id)
    .success(function(data) {
			$scope.evcredits=parseInt(data[data.length-1].tot_event_credits);
//			console.log($scope.creditsum);
			data.pop();
//			console.log(data);
      $scope.eventlist=data;
      });
		$http.get(root+'/api/getprojectcredits.php?id='+$scope.id)
    .success(function(data) {
     	$scope.prcredits=parseInt(data.project_credits);
//			console.log($scope.prcredits); 
      });
	$http.get(root+"/api/puller.php?id="+$scope.id)
	.	success(function(data){
		$scope.eventinfo = data;
		$scope.maxcredits=data.max_credits
		$scope.eventname= data.eventname;
		$scope.credits=data.credits;
		if ($scope.credits === null) {
			$scope.credits="Credits are not assigned";
		};
//		console.log(data);
		})
	}
	else{
		$location.path('/');
	}
}]);
// Start EventsCtrl
Controllers.controller('ProjectsCtrl',['$scope','$http','$cookies','$location',function($scope,$http,$cookies,$location){
$scope.RemoveCookie = function () {
		$cookies.remove('useradmin');
    $cookies.remove('fullname');
    $cookies.remove('hostel');
    $cookies.remove('id');
    $cookies.remove('usertype');
		$cookies.remove('usertype1');
    $scope.fullname='';
    $scope.userid='';
		$scope.useradmin=false;
		$scope.leader=false;
    $location.path('/projects');
  };
	$scope.leader=false;
	$scope.id=$cookies.get("id");
//	console.log($scope.id); 
    if($scope.id!=null){
      $scope.loggedin=true;
			$scope.fullname=$cookies.get("fullname");
			if($cookies.get("usertype1")=="leader")
				$scope.leader=true;
			else
			{
	//			console.log("wtf");
				$scope.leader=false;	
			}
    }

    else{
      $scope.loggedin=false;
    }
	//console.log($cookies.get("usertype1"));
//	console.log("1"+$cookies.get('useradmin'));
	if($cookies.get('useradmin')!=null || $cookies.get('useradmin')!=undefined)
		$scope.useradmin=$cookies.get('useradmin');
	else
		$scope.useradmin=false;
	console.log($scope.useradmin);
if($scope.id != null){
		$http.get(root+'/api/puller2.php')
		.success(function(data) {
    	$scope.events = data;
  		});
		};
/*	if ($scope.userid=="3672" || $scope.userid=="8878") {
		$scope.useradmin= true;
	}
	else{
		$scope.useradmin=true;
	}*/

		$scope.updatecredit= function(projectname,credits){
		$http.get(root+"/api/updateprojcredits.php?projectname="+projectname+"&credits="+credits)
		.success(function(data){
//			$scope.note="Succesfully updated";
			alert("Updated succesfully");
		})
};
$scope.getprojectdetails = function(projectname){
	$http.get(root+"/api/projectdetails.php?projectname="+projectname)
	.success(function(data){
		$scope.projectdetails=data;
	})
};
$scope.t1=false;
$scope.t2=true;
$scope.t3=true;
$scope.t4=true;
$http.get(root+"/api/projectscategory.php?cat=Content generation/translation")
	.success(function(data){
		$scope.projectslist=data;
	});
$http.get(root+"/api/projectdetails.php?projectname=Education via Blogging")
	.success(function(data){
		$scope.projectdetails=data;
//		console.log(data);
	});
$scope.category = function(cat){
	var cate="";
	if (cat=="cat1") {cate = document.getElementById("cat1").innerHTML;$scope.t1=false;$scope.t2=true;$scope.t3=true;$scope.t4=true;}
	else if (cat=="cat2") {cate = document.getElementById("cat2").innerHTML;$scope.t1=true;$scope.t2=false;$scope.t3=true;$scope.t4=true;}
	else if (cat=="cat3") {cate = document.getElementById("cat3").innerHTML;$scope.t1=true;$scope.t2=true;$scope.t3=false;$scope.t4=true;}
	else if (cat=="cat4") {cate = document.getElementById("cat4").innerHTML;$scope.t1=true;$scope.t2=true;$scope.t3=true;$scope.t4=false;}
	else{cate="null"};
	$http.get(root+"/api/projectscategory.php?cat="+cate)
	.success(function(data){
		$scope.projectslist=data;
	});
};
}]);


// End EventsCtrl
Controllers.controller('uploadCtrl',['$scope','$http','$cookies','$location',function($scope,$http,$cookies,$location){
	$scope.RemoveCookie = function () {
		$cookies.remove('usertype1');
    $cookies.remove('fullname');
    $cookies.remove('hostel');
    $cookies.remove('id');
    $cookies.remove('usertype');
//		$cookies.remove('useradmin');
    $scope.fullname='';
    $scope.userid='';
    $location.path('/');
  };
	
  $scope.rollno=$cookies.get("username");
  $scope.fullname=$cookies.get("fullname");
  $scope.hostel=$cookies.get("hostel");
	$scope.id=$cookies.get("id");
	if($scope.id!=null){
      $scope.loggedin=true;
			$scope.fullname=$cookies.get("fullname");
			if($cookies.get("usertype1")=="leader")
				$scope.leader=true;
			else
			{
		//		console.log("wtf");
				$scope.leader=false;
				$location.path('/');
			}
    }

    else{
			$location.path('/');
      $scope.loggedin=false;
    }

/*	$http.get('https://nss.iitm.ac.in//api/leaders.php?id='+$scope.id)
    .success(function(data){
      $scope.leader=data;
  //      console.log(data);
    });*/
	 $http.get(root+'/api/getlist.php?id='+$scope.id)
    .success(function(data){
      $scope.vols=data;
  //     console.log(data);
    });
		$scope.updatecredit= function(volname,eventname,credits,reason){
//		console.log(reason);
    $http.get(root+"/api/updatevolcredits.php?volname="+volname+"&eventname="+eventname+"&credits="+credits+"&reason="+reason)
    .success(function(data){
       alert("Record succesfully updated");
  //    console.log(data);
    })
		};
		$http.get(root+"/api/puller.php?id="+$scope.id)
  	.success(function(data){
    	$scope.maxcredits=data[0].max_credits;
		//	console.log($scope.maxcredits);
		});
}]);

